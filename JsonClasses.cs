﻿using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace paymentdeclinereportgeneration
{
    #region Pd Request Class
    public partial class PdRequestJson
    {
        [JsonProperty("aggs")]
        public PdRequestJsonAggs Aggs { get; set; }

        [JsonProperty("query")]
        public Query Query { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }
    }

    public partial class PdRequestJsonAggs
    {
        [JsonProperty("group_by_failed")]
        public PurpleGroupByFailed GroupByFailed { get; set; }
    }

    public partial class PurpleGroupByFailed
    {
        [JsonProperty("terms")]
        public Terms Terms { get; set; }

        [JsonProperty("aggs")]
        public GroupByFailedAggs Aggs { get; set; }
    }

    public partial class GroupByFailedAggs
    {
        [JsonProperty("group_by_status")]
        public GroupByStatus GroupByStatus { get; set; }
    }
    public partial class GroupByStatus
    {
        [JsonProperty("terms")]
        public Terms Terms { get; set; }

        [JsonProperty("aggs")]
        public GroupByStatusAggs Aggs { get; set; }
    }
    public partial class GroupByStatusAggs
    {
        [JsonProperty("group_by_clients")]
        public GroupByClients GroupByClients { get; set; }
    }
    public partial class GroupByClients
    {
        [JsonProperty("terms")]
        public Terms Terms { get; set; }

        [JsonProperty("aggs")]
        public GroupByClientsAggs Aggs { get; set; }
    }

    public partial class GroupByClientsAggs
    {
        [JsonProperty("group_by_failed")]
        public FluffyGroupByFailed GroupByFailed { get; set; }
    }

    public partial class FluffyGroupByFailed
    {
        [JsonProperty("terms")]
        public Terms Terms { get; set; }
    }


    public partial class Terms
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }
    }

    public partial class Query
    {
        [JsonProperty("bool")]
        public Bool Bool { get; set; }
    }

    public partial class Bool
    {
        [JsonProperty("must")]
        public List<Must> Must { get; set; }
    }

    public partial class Must
    {
        [JsonProperty("query_string", NullValueHandling = NullValueHandling.Ignore)]
        public QueryString QueryString { get; set; }

        [JsonProperty("range", NullValueHandling = NullValueHandling.Ignore)]
        public Range Range { get; set; }
    }

    public partial class QueryString
    {
        [JsonProperty("query")]
        public string Query { get; set; }
    }

    public partial class Range
    {
        [JsonProperty("sixt4.Timestamp")]
        public Sixt4Timestamp Sixt4Timestamp { get; set; }
    }

    public partial class Sixt4Timestamp
    {
        [JsonProperty("gte")]
        public string Gte { get; set; }

        [JsonProperty("lte")]
        public string Lte { get; set; }
        [JsonProperty("format")]
        public string Format { get; set; }
    }

    #endregion

    #region Pd Response Json
    public partial class PdResponseJson
    {
        [JsonProperty("took")]
        public long Took { get; set; }

        [JsonProperty("timed_out")]
        public bool TimedOut { get; set; }

        [JsonProperty("_shards")]
        public Shards Shards { get; set; }

        [JsonProperty("hits")]
        public Hits Hits { get; set; }

        [JsonProperty("aggregations")]
        public Aggregations Aggregations { get; set; }
    }

    public partial class Aggregations
    {
        [JsonProperty("group_by_failed")]
        public AggregationsGroupByFailed GroupByFailed { get; set; }
    }

    public partial class AggregationsGroupByFailed
    {
        [JsonProperty("doc_count_error_upper_bound")]
        public long DocCountErrorUpperBound { get; set; }

        [JsonProperty("sum_other_doc_count")]
        public long SumOtherDocCount { get; set; }

        [JsonProperty("buckets")]
        public PurpleBucket[] Buckets { get; set; }
    }

    public partial class PurpleBucket
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("doc_count")]
        public long DocCount { get; set; }

        [JsonProperty("group_by_status")]
        public GroupByStatusR GroupByStatus { get; set; }
    }

    public partial class GroupByStatusR
    {
        [JsonProperty("doc_count_error_upper_bound")]
        public long DocCountErrorUpperBound { get; set; }

        [JsonProperty("sum_other_doc_count")]
        public long SumOtherDocCount { get; set; }

        [JsonProperty("buckets")]
        public GroupByStatusBucket[] Buckets { get; set; }
    }

    public partial class GroupByStatusBucket
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("doc_count")]
        public long DocCount { get; set; }

        [JsonProperty("group_by_clients")]
        public GroupByClientsR GroupByClients { get; set; }
    }

    public partial class GroupByClientsR
    {
        [JsonProperty("doc_count_error_upper_bound")]
        public long DocCountErrorUpperBound { get; set; }

        [JsonProperty("sum_other_doc_count")]
        public long SumOtherDocCount { get; set; }

        [JsonProperty("buckets")]
        public GroupByClientsBucket[] Buckets { get; set; }
    }

    public partial class GroupByClientsBucket
    {
        [JsonProperty("key")]
        public string FluffyKey_Key { get; set; }

        [JsonProperty("doc_count")]
        public long DocCount { get; set; }

        [JsonProperty("group_by_failed")]
        public BucketGroupByFailed GroupByFailed { get; set; }
    }

    public partial class BucketGroupByFailed
    {
        [JsonProperty("doc_count_error_upper_bound")]
        public long DocCountErrorUpperBound { get; set; }

        [JsonProperty("sum_other_doc_count")]
        public long SumOtherDocCount { get; set; }

        [JsonProperty("buckets")]
        public FluffyBucket[] Buckets { get; set; }
    }

    public partial class FluffyBucket
    {
        [JsonProperty("key")]
        public string PurpleKey_Key { get; set; }

        [JsonProperty("doc_count")]
        public long DocCount { get; set; }
    }

    public partial class Hits
    {
        [JsonProperty("total")]
        public long Total { get; set; }

        [JsonProperty("max_score")]
        public long MaxScore { get; set; }

        [JsonProperty("hits")]
        public object[] HitsHits { get; set; }
    }

    public partial class Shards
    {
        [JsonProperty("total")]
        public long Total { get; set; }

        [JsonProperty("successful")]
        public long Successful { get; set; }

        [JsonProperty("skipped")]
        public long Skipped { get; set; }

        [JsonProperty("failed")]
        public long Failed { get; set; }
    }

    // public enum PurpleKey { EfoeRecipesSharedExceptionsAccessDeniedException, EfoeRecipesSharedExceptionsFareNotFoundException, EfoeRecipesSharedExceptionsPaymentDeclinedException, EfoeRecipesSharedExceptionsThreeDSecureException, EfoeRecipesSharedExceptionsUnexpectedStateException, EfoeRecipesSharedExceptionsUnsolvedCaptchaException, OpenQaSeleniumElementClickInterceptedException, OpenQaSeleniumElementNotInteractableException, OpenQaSeleniumElementNotVisibleException, OpenQaSeleniumUnhandledAlertException, OpenQaSeleniumWebDriverException, SystemArgumentNullException, SystemArgumentOutOfRangeException, SystemException, SystemInvalidOperationException, SystemNullReferenceException, SystemOperationCanceledException, SystemThreadingTasksTaskCanceledException };

    //public enum FluffyKey { AirfareTest, Alternativeairlines, Democlient, Etraveli, EtraveliTest, Flightnetwork, Lbftravel, PaxportTest, Skytours, Travelqube, Travelstart, Travofy, Tripstack };

    //public enum TentacledKey { Completed, Failed, Started };
    #endregion

    #region PDDetail Request Json

    public partial class PddRequestJson
    {
        [JsonProperty("query")]
        public Query Query { get; set; }

        [JsonProperty("_source")]
        public Source Source { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }
    }

    public partial class Source
    {
        [JsonProperty("includes")]
        public List<string> Includes { get; set; }
    }

    #endregion

    #region PDDetail Response Json

    public partial class PddResponseJson
    {
        [JsonProperty("took")]
        public long Took { get; set; }

        [JsonProperty("timed_out")]
        public bool TimedOut { get; set; }

        [JsonProperty("_shards")]
        public Shards Shards { get; set; }

        [JsonProperty("hits")]
        public Hits2 Hits { get; set; }
    }

    public partial class Hits2
    {
        [JsonProperty("total")]
        public long Total { get; set; }

        [JsonProperty("max_score")]
        public double MaxScore { get; set; }

        [JsonProperty("hits")]
        public List<Hit> HitsHits { get; set; }
    }

    public partial class Hit
    {
        [JsonProperty("_index")]
        public string Index { get; set; }

        [JsonProperty("_type")]
        public string Type { get; set; }

        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("_score")]
        public double Score { get; set; }

        [JsonProperty("_source")]
        public SourceR Source { get; set; }
    }

    public partial class SourceR
    {
        [JsonProperty("sixt4")]
        public Sixt4 Sixt4 { get; set; }
    }

    public partial class Sixt4
    {
        [JsonProperty("State")]
        public State State { get; set; }
    }

    public partial class State
    {
        [JsonProperty("BookCriteria", NullValueHandling=NullValueHandling.Ignore)]
        public BookCriteria BookCriteria { get; set; }

        [JsonProperty("SessionId", NullValueHandling=NullValueHandling.Ignore)]
        public string SessionId { get; set; }

        [JsonProperty("SearchCriteria", NullValueHandling=NullValueHandling.Ignore)]
        public SearchCriteria SearchCriteria { get; set; }

        [JsonProperty("Tags", NullValueHandling=NullValueHandling.Ignore)]
        public Tags Tags { get; set; }
    }

    public partial class BookCriteria
    {
        [JsonProperty("PaymentInformation", NullValueHandling=NullValueHandling.Ignore)]
        public PaymentInformation PaymentInformation { get; set; }

        [JsonProperty("Adults", NullValueHandling=NullValueHandling.Ignore)]
        public List<Adult> Adults { get; set; }

        [JsonProperty("ContactInformation", NullValueHandling=NullValueHandling.Ignore)]
        public ContactInformation ContactInformation { get; set; }
    }

    public partial class Adult
    {
        [JsonProperty("Nationality")]
        public string Nationality { get; set; }
    }

    public partial class ContactInformation
    {
        [JsonProperty("Email", NullValueHandling=NullValueHandling.Ignore)]
        public string Email { get; set; }

        [JsonProperty("PhoneNumber", NullValueHandling=NullValueHandling.Ignore)]
        public string PhoneNumber { get; set; }
    }

    public partial class PaymentInformation
    {
        [JsonProperty("CardHolderLastName", NullValueHandling=NullValueHandling.Ignore)]
        public string CardHolderLastName { get; set; }

        [JsonProperty("CardNumber", NullValueHandling=NullValueHandling.Ignore)]
        public string CardNumber { get; set; }

        [JsonProperty("PostalCode", NullValueHandling=NullValueHandling.Ignore)]
        public string PostalCode { get; set; }

        [JsonProperty("Country", NullValueHandling=NullValueHandling.Ignore)]
        public string Country { get; set; }

        [JsonProperty("CardHolderFirstName", NullValueHandling=NullValueHandling.Ignore)]
        public string CardHolderFirstName { get; set; }

        [JsonProperty("Address1", NullValueHandling=NullValueHandling.Ignore)]
        public string Address1 { get; set; }
    }

    public partial class SearchCriteria
    {
        [JsonProperty("FromCode")]
        public string FromCode { get; set; }

        [JsonProperty("Currency")]
        public string Currency { get; set; }

        [JsonProperty("ToCode")]
        public string ToCode { get; set; }

        [JsonProperty("DepartureDate")]
        public string DepartureDate { get; set; }
    }

    public partial class Tags
    {
        [JsonProperty("ClientName")]
        public string ClientName { get; set; }
    }

    #endregion

    #region NoneAddrRequestJson 

    public partial class NoneAddrRequestJson1
    {
        [JsonProperty("aggs")]
        public NoneAddrRequestJson1Aggs Aggs { get; set; }

        [JsonProperty("query")]
        public QueryNQ Query { get; set; }

        [JsonProperty("size")]
        public int Size{ get; set; }
    }

    public partial class NoneAddrRequestJson1Aggs
    {
        [JsonProperty("group_by_lcc")]
        public GroupByLcc GroupByLcc { get; set; }
    }

    public partial class GroupByLcc
    {
        [JsonProperty("terms")]
        public Terms Terms { get; set; }

        [JsonProperty("aggs")]
        public GroupByLccAggs Aggs { get; set; }
    }

    public partial class GroupByLccAggs
    {
        [JsonProperty("group_by_status")]
        public GroupByStatusNQ GroupByStatus { get; set; }
    }

    public partial class GroupByStatusNQ
    {
        [JsonProperty("terms")]
        public Terms Terms1 { get; set; }

        [JsonProperty("aggs")]
        public GroupByStatusAggsNQ statusAggs { get; set; }
    }

    public partial class GroupByStatusAggsNQ
    {
        [JsonProperty("group_by_failed")]
        public GroupByFailed GroupByFailed { get; set; }
    }

    public partial class GroupByFailed
    {
        [JsonProperty("terms")]
        public Terms Terms { get; set; }
    }

    public partial class QueryNQ
    {
        [JsonProperty("bool")]
        public Bool Bool { get; set; }
    }

    #endregion

    #region NoneAddressResponseJson

    public partial class NoneAddressResponseJson
    {
        [JsonProperty("took")]
        public long Took { get; set; }

        [JsonProperty("timed_out")]
        public bool TimedOut { get; set; }

        [JsonProperty("_shards")]
        public Shards Shards { get; set; }

        [JsonProperty("hits")]
        public Hits Hits { get; set; }

        [JsonProperty("aggregations")]
        public AggregationsNQ Aggregations { get; set; }
    }

    public partial class AggregationsNQ
    {
        [JsonProperty("group_by_lcc")]
        public GroupByLccNQR GroupByLcc { get; set; }
    }

    public partial class GroupByLccNQR
    {
        [JsonProperty("doc_count_error_upper_bound")]
        public long DocCountErrorUpperBound { get; set; }

        [JsonProperty("sum_other_doc_count")]
        public long SumOtherDocCount { get; set; }

        [JsonProperty("buckets")]
        public List<GroupByLccBucketNQR> Buckets { get; set; }
    }

    public partial class GroupByLccBucketNQR
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("doc_count")]
        public long DocCount { get; set; }

        [JsonProperty("group_by_status")]
        public GroupByStatusNQR GroupByStatus { get; set; }
    }

    public partial class GroupByStatusNQR
    {
        [JsonProperty("doc_count_error_upper_bound")]
        public long DocCountErrorUpperBound { get; set; }

        [JsonProperty("sum_other_doc_count")]
        public long SumOtherDocCount { get; set; }

        [JsonProperty("buckets")]
        public List<GroupByStatusBucketNQR> Buckets { get; set; }
    }

    public partial class GroupByStatusBucketNQR
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("doc_count")]
        public long DocCount { get; set; }

        [JsonProperty("group_by_failed")]
        public GroupByFailedNQR GroupByFailed { get; set; }
    }

    public partial class GroupByFailedNQR
    {
        [JsonProperty("doc_count_error_upper_bound")]
        public long DocCountErrorUpperBound { get; set; }

        [JsonProperty("sum_other_doc_count")]
        public long SumOtherDocCount { get; set; }

        [JsonProperty("buckets")]
        public List<GroupByFailedBucketNQR> Buckets { get; set; }
    }

    public partial class GroupByFailedBucketNQR
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("doc_count")]
        public long DocCount { get; set; }
    }

    #endregion

    #region NoneAddressRequestJson2

    public partial class NoneAddrRequestJson2
    {
        [JsonProperty("aggs")]
        public NoneAddrRequestJson2Aggs Aggs { get; set; }

        [JsonProperty("query")]
        public Query Query { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }
    }

    public partial class NoneAddrRequestJson2Aggs
    {
        [JsonProperty("group_by_status")]
        public GroupByStatusNQ GroupByStatus { get; set; }
    }

    #endregion

    #region NoneAddressResponseJson2
   
    public partial class NoneAddrResponseJson2
    {
        [JsonProperty("took")]
        public long Took { get; set; }

        [JsonProperty("timed_out")]
        public bool TimedOut { get; set; }

        [JsonProperty("_shards")]
        public Shards Shards { get; set; }

        [JsonProperty("hits")]
        public Hits Hits { get; set; }

        [JsonProperty("aggregations")]
        public AggregationsNQ2 Aggregations { get; set; }
    }

    public partial class AggregationsNQ2
    {
        [JsonProperty("group_by_status")]
        public GroupByStatusNQR GroupByStatus { get; set; }
    }

    #endregion

    #region Bin Query Response Json

    public partial class BinQueryResponse
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("bin")]
        public Bin Bin { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("brand")]
        public string Brand { get; set; }

        [JsonProperty("issuer")]
        public string Issuer { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public partial class Bin
    {
        [JsonProperty("gte")]
        public double Gte { get; set; }

        [JsonProperty("lte")]
        public double Lte { get; set; }
    }

    #endregion

}
