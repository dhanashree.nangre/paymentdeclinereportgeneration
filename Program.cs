﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;


namespace paymentdeclinereportgeneration
{
    public class Program
    {
        public static Tool toolObj;


        public static void Main(string[] args)
        {
            var LastweekLCCPDpercentage = new List<PdLcc>();
            var PreviousweekLCCPDpercentage = new List<PdLcc>();
            var Last30dayLCCPDpercentage = new List<PdLcc>();
            toolObj = new Tool();
            LastweekLCCPDpercentage = toolObj.GetHighestPDLCCs(0, 7);
            List<Decline> ClientPDpercentage = toolObj.GetPDClient(LastweekLCCPDpercentage);
            FileHandling.WriteToExcelFile(LastweekLCCPDpercentage.ToList(), ClientPDpercentage.ToList(), 0, 7);
            PreviousweekLCCPDpercentage = toolObj.GetHighestPDLCCs(7, 14);
            List<Decline> PreviousClientPDpercentage = toolObj.GetPDClient(PreviousweekLCCPDpercentage);
            Last30dayLCCPDpercentage = toolObj.GetHighestPDLCCs(0, 30);
            FileHandling.WriteToExcelFile(PreviousweekLCCPDpercentage.ToList(), PreviousClientPDpercentage.ToList(), 7, 14);
            List<Decline> Last30dayClientPDpercentage = toolObj.GetPDClient(Last30dayLCCPDpercentage);
            FileHandling.WriteToExcelFile(Last30dayLCCPDpercentage.ToList(), Last30dayClientPDpercentage, 0, 30);
            email_send(7, LastweekLCCPDpercentage,PreviousweekLCCPDpercentage,Last30dayLCCPDpercentage, ClientPDpercentage,PreviousClientPDpercentage,Last30dayClientPDpercentage);
        }


        public static void email_send(int dayRange,List<PdLcc> LCC_7, List <PdLcc> LCC_7_14, List<PdLcc> LCC_30,List<Decline> Client_7, List<Decline> Client_7_14, List<Decline> Client_30)
        {
           
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.tpstk.ca", 25);

            //SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("dhanashree.nangre@tripstack.com");
            mail.To.Add("dhanashree.nangre@tripstack.com");
            //mail.To.Add("fernando.montoro@tripstack.com ");
            //mail.To.Add("alex.colici@tripstack.com ");
            //mail.To.Add("cx@tripstack.com");
            mail.Subject =$"Payment declines Report from {DateTime.Now.AddDays(-7).ToString("MMMM dd, yyyy")} to {DateTime.Now.AddDays(-1).ToString("MMMM dd, yyyy")} ";
            string mailbody = getTable(Client_7, Client_7_14, Client_30, "Client") + "<br>" + getLCCTable(LCC_7, LCC_7_14, LCC_30, "LCC");

            mail.Body = mailbody;
            mail.IsBodyHtml = true;
            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment($"C:/Project/paymentdeclinereportgeneration/bin/Debug/netcoreapp2.1/data/query1/results_for_last_{DateTime.Now.AddDays(-7).ToString("MMMM dd, yyyy")}_to{DateTime.Now.AddDays(-1).ToString("MMMM dd, yyyy")}_days.xlsx");
            mail.Attachments.Add(attachment);
            System.Net.Mail.Attachment attachment1 = new System.Net.Mail.Attachment($"C:/Project/paymentdeclinereportgeneration/bin/Debug/netcoreapp2.1/data/query1/results_for_last_{DateTime.Now.AddDays(-30).ToString("MMMM dd, yyyy")}_to{DateTime.Now.AddDays(-1).ToString("MMMM dd, yyyy")}_days.xlsx");
            mail.Attachments.Add(attachment1);
           //SmtpServer.Port = 587;
           //  SmtpServer.EnableSsl = true;
           SmtpServer.UseDefaultCredentials = true;

            SmtpServer.Send(mail);
            /*MailMessage mailMessage = new MailMessage();
mailMessage.From = new MailAddress("blog@vsysad.com");
mailMessage.To.Add("thejapinator@gmail.com");
mailMessage.Subject = txtSubject.Text;
 
mailMessage.Body = "<b>Sender Name : </b>" + txtName.Text + "<br/>"
     + "<b>Sender Email : </b>" + txtEmail.Text + "<br/>"
     + "<b>Comments : </b>" + txtComments.Text;
mailMessage.IsBodyHtml = true;
 
SmtpClient smtpClient = new SmtpClient("localhost", 25);
smtpClient.Send(mailMessage);
*/

        }
        public static string getcolourcode(decimal percentage)
        {
            string colour_code = "";
            if (percentage < 10)
            {
                colour_code = "#D6F3BF";
            }
            else if (10 < percentage && percentage < 20)
            {
                colour_code = "#FCFBBE";
            }
            else
            {
                colour_code = "#FCD5BE";
            }
            return colour_code;
        }
        public static string getdifference(decimal diff)
        {
            string value = "";
            if(diff<=0)
            {
                diff = diff * (-1);
                value = "↑ " + diff.ToString();
            }
            else
            {
                value = "↓ " + diff.ToString();
            }
            return value;
        }
        public static string getTable(List<Decline> obj, List<Decline> obj1, List<Decline> obj2, string Type)
        {
            string textBody = " <table ' border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 1000 + "><tr bgcolor='#F6CDFA'><td></td><td colspan='3' style='text-align:center'><b>Last week</b></td><td colspan='3' style='text-align:center'><b>Previous week</b></td><td colspan='3' style='text-align:center'><b>Last 30 days</b></td><td><td></tr>"
                + "<tr bgcolor='#CDE9FA'><td style='text-align:center'><b>Client</b></td> <td style='text-align:center'> <b> Total Booking attempts</b> </td><td style='text-align:center'> <b> Total Declines</b> </td><td style='text-align:center'> <b> Total Declines %</b> </td><td style='text-align:center'> <b> Total Booking attempts</b> </td><td style='text-align:center'> <b> Total Declines</b> </td><td style='text-align:center'> <b> Total Declines %</b> </td><td style='text-align:center'> <b> Total Booking attempts</b> </td><td style='text-align:center'> <b> Total Declines</b> </td><td style='text-align:center'> <b> Total Declines %</b> </td><td style='text-align:center'> <b> Comparing this week to last week</b> </td><td style='text-align:center'> <b> Comparing this week to Last 30 days</b> </td></tr>";
            var clients = toolObj.Clients;
            clients.Add("Total");
                foreach (var client in clients)
                {
                    var client_7 = obj.Where(x => x.Name.Equals(client)).ToList();
                    var client_7_14 = obj1.Where(x => x.Name.Equals(client)).ToList();
                    var client_30 = obj2.Where(x => x.Name.Equals(client)).ToList();
                    string client_7_colour = getcolourcode(client_7[0].Declines_percentage);
                    string client_7_14_colour = getcolourcode(client_7_14[0].Declines_percentage);
                    string client_30_colour = getcolourcode(client_30[0].Declines_percentage);
                    decimal difference_Lastweek = client_7[0].Declines_percentage - client_7_14[0].Declines_percentage;
                    decimal difference_Last30days = client_7[0].Declines_percentage - client_30[0].Declines_percentage;
                    string differnce_color = difference_Lastweek <= 0 ? "#D6F3BF" : "#FCD5BE";
                    string differnce30_color = difference_Last30days <= 0 ? "#D6F3BF" : "#FCD5BE";
                textBody += "<tr><td style='text-align:center' bgcolor='#CDDEFA'><b>" + client + "</b></td><td style='text-align:center' bgcolor='#F3EEED'>" + client_7[0].Hits + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + client_7[0].Declines + "</td><td style='text-align:center' bgcolor='"+ client_7_colour + "'>" + client_7[0].Declines_percentage + "</td>";
                textBody += "<td style='text-align:center' bgcolor='#F3EEED'>" + client_7_14[0].Hits + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + client_7_14[0].Declines + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + client_7_14[0].Declines_percentage + "</td>";
                textBody += "<td style='text-align:center' bgcolor='#F3EEED'>" + client_30[0].Hits + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + client_30[0].Declines + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + client_30[0].Declines_percentage + "</td>";
                textBody += "<td style='text-align:center' bgcolor='" + differnce_color + "'>" + getdifference( difference_Lastweek) + "</td> <td style='text-align:center' bgcolor='" + differnce30_color + "'>" + getdifference(difference_Last30days) + "</td></tr>";
            }
            textBody += "</table>";
            return textBody;
        }
        public static string getLCCTable(List<PdLcc> LCC_7 , List<PdLcc> LCC_7_14, List<PdLcc> LCC_30,string type)
        {
            List<string> lccs = new List<string> {"spirit","frontier","norwegianapi","ryanair","wizzair","allegiant","volaris","suncountry","vietjet","vuelingapi","interjet","vivaaerobus",
                "blueair","scootapi","flair","swoop","jejuair","level","tuiflybe","eurowings" };

            string textBody = " <table ' border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 1000 + "><tr bgcolor='#F6CDFA'><td></td><td colspan='3' style='text-align:center'><b>Last week</b></td><td colspan='3' style='text-align:center'><b>Previous week</b></td><td colspan='3' style='text-align:center'><b>Last 30 days</b></td><td><td></tr>"
                + "<tr bgcolor='#CDE9FA'><td style='text-align:center'><b>Client</b></td> <td style='text-align:center'> <b> Total Booking attempts</b> </td><td style='text-align:center'> <b> Total Declines</b> </td><td style='text-align:center'> <b> Total Declines %</b> </td><td style='text-align:center'> <b> Total Booking attempts</b> </td><td style='text-align:center'> <b> Total Declines</b> </td><td style='text-align:center'> <b> Total Declines %</b> </td><td style='text-align:center'> <b> Total Booking attempts</b> </td><td style='text-align:center'> <b> Total Declines</b> </td><td style='text-align:center'> <b> Total Declines %</b> </td><td style='text-align:center'> <b> Comparing this week and last week</b> </td></td><td style='text-align:center'> <b> Comparing this week to Last 30 days</b> </td</tr>";
           
            foreach (var lcc in lccs)
            {
                try
                {
                    var lcc_7 = (LCC_7.Where(x => x.Lcc.Equals(lcc)).ToList())[0].PerClient.Where(x => x.Name.Equals("Total")).ToList();
                    List<Decline> lcc_7_14 = new List<Decline>();
                    try
                    {
                        //This LCC didn't made any booking last week
                        lcc_7_14 = (LCC_7_14.Where(x => x.Lcc.Equals(lcc)).ToList())[0].PerClient.Where(x => x.Name.Equals("Total")).ToList();
                    }
                    catch
                    {
                        lcc_7_14.Add(new Decline("Total", 0, 0, 0));
                    }
                    var lcc_30 = (LCC_30.Where(x => x.Lcc.Equals(lcc)).ToList())[0].PerClient.Where(x => x.Name.Equals("Total")).ToList();
                    string lcc_7_colour = getcolourcode(lcc_7[0].Declines_percentage);
                    string lcc_7_14_colour = getcolourcode(lcc_7_14[0].Declines_percentage);
                    string lcc_30_colour = getcolourcode(lcc_30[0].Declines_percentage);
                    decimal difference = lcc_7[0].Declines_percentage - lcc_7_14[0].Declines_percentage;
                    decimal difference_30 = lcc_7[0].Declines_percentage - lcc_30[0].Declines_percentage;
                    string differnce_color = difference <= 0 ? "#D6F3BF" : "#FCD5BE";
                    string differnce_30_color = difference_30 <= 0 ? "#D6F3BF" : "#FCD5BE";
                    textBody += "<tr><td style='text-align:center' bgcolor='#CDDEFA'><b>" + lcc + "</b></td><td style='text-align:center' bgcolor='#F3EEED'>" + lcc_7[0].Hits + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + lcc_7[0].Declines + "</td><td style='text-align:center' bgcolor='"+ lcc_7_colour + "'>" + lcc_7[0].Declines_percentage + "</td>";
                    textBody += "<td style='text-align:center' bgcolor='#F3EEED'>" + lcc_7_14[0].Hits + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + lcc_7_14[0].Declines + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + lcc_7_14[0].Declines_percentage + "</td>";
                    textBody += "<td style='text-align:center' bgcolor='#F3EEED'>" + lcc_30[0].Hits + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + lcc_30[0].Declines + "</td><td style='text-align:center' bgcolor='#F3EEED'>" + lcc_30[0].Declines_percentage + "</td>";
                    textBody += "<td style='text-align:center' bgcolor='" + differnce_color + "'>" + getdifference(difference) + "</td><td style='text-align:center' bgcolor='" + differnce_30_color + "'>" + getdifference( difference_30) + "</td></tr>";
                }
                catch
                {
                    continue;
                }
            }
            textBody += "</table>";
            return textBody;
        }
    }
}