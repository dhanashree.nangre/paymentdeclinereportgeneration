﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Xml.Schema;
using Newtonsoft.Json;
using Npgsql;

namespace paymentdeclinereportgeneration
{
    public class Tool
    {
       
        private string UriString = "https://es.logging.production.tripstack.com/avail-book-*/_search?source_content_type=application/json&source=";

        public int queryId = 0;

        HttpClient httpClient;

        ElasticSearchQueries elastic;
        

        private DataSet ds = new DataSet();
        public List<string> Clients = new List<string> {  "etraveli", "lbftravel", "alternativeairlines", "loveholidays", "flighthub" };
        public List<string> Clients_total = new List<string> { "etraveli", "lbftravel", "alternativeairlines", "loveholidays", "flighthub","Total" };
        public Tool()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(UriString);
            elastic = new ElasticSearchQueries();
        }

        public List<PdLcc> GetHighestPDLCCs(int from_day ,int to_day)
        {
            var PDpercentage = new List<PdLcc>();

            var requestObject = elastic.GetRequestObject(from_day, to_day, 0);

            var responseString = elastic.GetQueryResponse(requestObject, httpClient, UriString);

            if (string.IsNullOrEmpty(responseString) || responseString.Contains("No server is available to handle this request."))
            {
                return null;
            }

            PdResponseJson responseObject = JsonConvert.DeserializeObject<PdResponseJson>(responseString);

            foreach (var lcc in responseObject.Aggregations.GroupByFailed.Buckets)
            {
                List<Decline> PerClient = new List<Decline>();
                foreach (var client in Clients)
                {
                    var new_client = GetDecline(lcc, client);
                    PerClient.Add(new_client);
                }
                var Total_Hits = (decimal)lcc.GroupByStatus.Buckets.Where(x => x.Key.Equals("Started")).First().DocCount;
                var Ch_Failed = lcc.GroupByStatus.Buckets.Where(x => x.Key.Equals("Failed")).ToList().FirstOrDefault();
                var Client_Fails = Ch_Failed == null ? 0m : Ch_Failed.DocCount;
                var Client_Declines = PerClient.Sum(x => x.Declines);
                Decline LCC_total = new Decline("Total", Client_Fails, Total_Hits, Client_Declines);
                 PerClient.Add(LCC_total);
                
                PDpercentage.Add(new PdLcc(lcc.Key, PerClient));

            }
            return PDpercentage.ToList();

        }
        public List<Decline> GetPDClient(List<PdLcc> pdLccs)
        {
       
            List<Decline> Client_declines = new List<Decline>();
            var obj = pdLccs.Select(x => (PdLcc)x).ToList();
            
            foreach (var client in Clients_total)
            {
                decimal hits = 0;
                decimal fails = 0;
                decimal declines = 0;

                foreach (var lcc in pdLccs)
                {
                    hits = hits +(lcc.PerClient.Where(x => x.Name.Equals(client)).Select(x=>x.Hits).ToList())[0];
                    fails = fails+ (lcc.PerClient.Where(x => x.Name.Equals(client)).Select(x => x.Fails).ToList())[0];
                    declines =declines + (lcc.PerClient.Where(x => x.Name.Equals(client)).Select(x => x.Declines).ToList())[0];
                }
                Decline new_client = new Decline(client, fails, hits, declines);
                Client_declines.Add(new_client);
            }
            return Client_declines;

        }
        
        public static decimal get_percentage(decimal hits, decimal declines)
    {
        decimal percentage = hits==0?0m:(declines / hits) * 100;
            percentage = Decimal.Round(percentage, 2);
        return percentage;
    }
        public Decline GetDecline(PurpleBucket lcc, string client)
        {
             string Client_Name = client;
            decimal Client_Hits = 0m;
            decimal Client_Fails = 0m;
            decimal Client_Declines = 0m;
            var Ch_Started = lcc.GroupByStatus.Buckets.Where(x => x.Key.Equals("Started")).ToList().FirstOrDefault();
            var Ch_Failed = lcc.GroupByStatus.Buckets.Where(x => x.Key.Equals("Failed")).ToList().FirstOrDefault();
          
                if (Ch_Started != null && Ch_Failed != null && Ch_Started.GroupByClients.Buckets.Count() != 0)
                {
                    var choice_hits = Ch_Started.GroupByClients.Buckets.Where(x => x.FluffyKey_Key.Equals(client)).Select(x => x.DocCount).ToList();
                    Client_Hits = choice_hits.Count != 0 ? choice_hits[0] : 0m;
                }
                if (Ch_Failed != null && Ch_Failed.GroupByClients.Buckets.Count() != 0)
                {
                    var choice1 = Ch_Failed.GroupByClients.Buckets.Select(x => new KeyValuePair<string, BucketGroupByFailed>(x.FluffyKey_Key, x.GroupByFailed)).ToList();
                    foreach (var exception in choice1)
                    {
                        var Ch_declined = exception.Value.Buckets.Where(x => x.PurpleKey_Key.Equals("Efoe.Recipes.Shared.Exceptions.PaymentDeclinedException")).ToList().FirstOrDefault();
                        decimal DocCount = Ch_declined == null ? 0m : Ch_declined.DocCount;
                        if (exception.Key == client)
                        {
                            Client_Declines = DocCount;
                            break;
                        }
                    }
                }
                if (Ch_Failed != null)
                {
                    var choice_failed = Ch_Failed != null ? Ch_Failed.GroupByClients.Buckets.Where(x => x.FluffyKey_Key.Equals(client)).Select(x => x.DocCount).ToList() : null;
                    Client_Fails = choice_failed.Count == 0 ? 0m : choice_failed[0];
                }
                else
                {
                    Client_Fails = 0m;
                }
  
            Decline DeclinesPerClient = new Decline(Client_Name,Client_Fails,Client_Hits,Client_Declines);
            return DeclinesPerClient;
        }
    }
    public class Decline
    {
        public string Name;
        public decimal Fails;
        public decimal Hits;
        public decimal Declines;
        public decimal Declines_percentage;
        public Decline(string name, decimal fails, decimal hits, decimal declines)
        {
            Name = name;
            Fails = fails;
            Hits = hits;
            Declines = declines;
            decimal percentage = hits == 0m ? 0m:declines * 100 / hits;
            percentage = Decimal.Round(percentage, 2);
            Declines_percentage = percentage;
        }
    }


    public class PdLcc
    {
        public string Lcc;
        public List<Decline> PerClient = new List<Decline>();

        public PdLcc(string lcc, List<Decline> perClient )
        {
            Lcc = lcc;
            PerClient = perClient;

        }
    }
    public class PDClient
    {
        public List<Decline> PerClient = new List<Decline>();
        public PDClient(List<Decline> perClient)
        {
          PerClient = perClient;
        }
      
    }
}

