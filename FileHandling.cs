﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace paymentdeclinereportgeneration
{
    public static class FileHandling
    {
        public static int lastWrittenFilesCount = 0;
         public static List<string> order = new List<string> { "Total", "etraveli", "lbftravel", "alternativeairlines", "loveholidays", "flighthub" };
        /*
        public static void WriteToFile(List<PdLcc> sortedDict, List<Decline> Client, int from_day, int to_day)
        {
            string path = $@"data\query1\results_for_last_{DateTime.Now.AddDays(-to_day).ToString("MMMM dd, yyyy")}_to{DateTime.Now.AddDays(-1 - from_day).ToString("MMMM dd, yyyy")}_days.csv";
            if (!Directory.Exists($@"data\query1"))
            {
                DirectoryInfo LocalDirectory = Directory.CreateDirectory($@"data\query1");
            }
            var fileContents = new List<string>();
          string Client_string = ","+get_Client_string(Client);
            string header = string.Join(",", "Airline", "Total_booking_attempts", "Total_Fails", "Total_Declines", "Total_Declines_%", "FN_Total_booking_attempts", "FN_Total_Fails", "FN_Total_Declines", "FN_Total_Declines_%", "ETG_Total_booking_attempts", "ETG_Total_Fails", "ETG_Total_Declines", "ETG_Total_Declines_%", "LBF_Total_booking_attempts", "LBF_Total_Fails", "LBF_Total_Declines", "LBF_Total_Declines_%", "ALT_Total_booking_attempts", "ALT_Total_Fails", "ALT_Total_Declines", "ALT_Total_Declines_%", "LH_Total_booking_attempts", "LH_Total_Fails", "LH_Total_Declines", "LH_Total_Declines_%", "FH_Total_booking_attempts", "FH_Total_Fails", "FH_Total_Declines", "FH_Total_Declines_%");
            var result = get_LCC_string(sortedDict);
            fileContents.Add(header);
            fileContents.Add(Client_string);

            File.WriteAllLines(path, fileContents.ToArray().Concat(result.ToArray()), Encoding.UTF8);

        }*/
        public static void WriteToExcelFile(List<PdLcc> sortedDict, List<Decline> Client, int from_day, int to_day)
        {
            string path = $@"data\query1\results_for_last_{DateTime.Now.AddDays(-to_day).ToString("MMMM dd, yyyy")}_to{DateTime.Now.AddDays(-1 - from_day).ToString("MMMM dd, yyyy")}_days.xlsx";
            File.Delete(path);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            FileInfo SpreadsheetInfo = new FileInfo(path);

            ExcelPackage excel = new ExcelPackage(SpreadsheetInfo);

            var Declines = excel.Workbook.Worksheets.Add($"Declines_{from_day}_{to_day}");

            Declines.Cells[1, 1].Value ="Airline";
            Declines.Cells[1, 2].Value = "Total booking Attempts";
            Declines.Cells[1, 3].Value = "Total fails";
            Declines.Cells[1, 4].Value = "Total Declines";
            Declines.Cells[1, 5].Value = "Total Declines %";
            Declines.Cells[1, 6].Value = "ETG booking Attempts";
            Declines.Cells[1, 7].Value = "ETG fails";
            Declines.Cells[1, 8].Value = "ETG Declines";
            Declines.Cells[1, 9].Value = "ETG Declines %";
            Declines.Cells[1, 10].Value = "LBF booking Attempts";
            Declines.Cells[1, 11].Value = "LBF fails";
            Declines.Cells[1, 12].Value = "LBF Declines";
            Declines.Cells[1, 13].Value = "LBF Declines %";
            Declines.Cells[1, 14].Value = "ALternative booking Attempts";
            Declines.Cells[1, 15].Value = "ALternative fails";
            Declines.Cells[1, 16].Value = "ALternative Declines";
            Declines.Cells[1, 17].Value = "ALternative Declines %";
            Declines.Cells[1, 18].Value = "Loveholidays booking Attempts";
            Declines.Cells[1, 19].Value = "Loveholidays fails";
            Declines.Cells[1, 20].Value = "Loveholidays Declines";
            Declines.Cells[1, 21].Value = "Loveholidays Declines %";
            Declines.Cells[1, 22].Value = "Flighthub booking Attempts";
            Declines.Cells[1, 23].Value = "Flighthub fails";
            Declines.Cells[1, 24].Value = "Flighthub Declines";
            Declines.Cells[1, 25].Value = "Flighthub Declines %";
            Declines.Cells[2, 1].Value = "";
            int i = 2;
            foreach (var client_name in order)
            {
                var Client_value = Client.Where(x => x.Name.Equals(client_name)).ToList();
                Declines.Cells[2, i].Value = Client_value[0].Hits;
                Declines.Cells[2, i+1].Value = Client_value[0].Fails;
                Declines.Cells[2, i+2].Value = Client_value[0].Declines;
                Declines.Cells[2, i+3].Value = Client_value[0].Declines_percentage;
                i = i + 3;

            }/*
            Declines.Cells[2, 2].Value = Client[5].Hits;
            Declines.Cells[2, 3].Value = Client[5].Fails;
            Declines.Cells[2, 4].Value = Client[5].Declines;
            Declines.Cells[2, 5].Value = Client[5].Declines_percentage;
            Declines.Cells[2, 6].Value = Client[0].Hits;
            Declines.Cells[2, 7].Value = Client[0].Fails;
            Declines.Cells[2, 8].Value = Client[0].Declines;
            Declines.Cells[2, 9].Value = Client[0].Declines_percentage;
            Declines.Cells[2, 10].Value = Client[1].Hits;
            Declines.Cells[2, 11].Value = Client[1].Fails;
            Declines.Cells[2, 12].Value = Client[1].Declines;
            Declines.Cells[2, 13].Value = Client[1].Declines_percentage;
            Declines.Cells[2, 14].Value = Client[2].Hits;
            Declines.Cells[2, 15].Value = Client[2].Fails;
            Declines.Cells[2, 16].Value = Client[2].Declines;
            Declines.Cells[2, 17].Value = Client[2].Declines_percentage;
            Declines.Cells[2, 18].Value = Client[3].Hits;
            Declines.Cells[2, 19].Value = Client[3].Fails;
            Declines.Cells[2, 20].Value = Client[3].Declines;
            Declines.Cells[2, 21].Value = Client[3].Declines_percentage;
            Declines.Cells[2, 22].Value = Client[4].Hits;
            Declines.Cells[2, 23].Value = Client[4].Fails;
            Declines.Cells[2, 24].Value = Client[4].Declines;
            Declines.Cells[2, 25].Value = Client[4].Declines_percentage;*/
            int rowcount = 3;
            int lcc_count = 0;
            foreach (var lcc in sortedDict) 
            { 
                    Declines.Cells[rowcount, 1].Value = sortedDict[lcc_count].Lcc;
                int j = 2;
                foreach (var client_name in order)
                {
                    var Client_value = sortedDict[lcc_count].PerClient.Where(x => x.Name.Equals(client_name)).ToList();
                    Declines.Cells[rowcount, j].Value = Client_value[0].Hits;
                    Declines.Cells[rowcount, j + 1].Value = Client_value[0].Fails;
                    Declines.Cells[rowcount, j + 2].Value = Client_value[0].Declines;
                    Declines.Cells[rowcount, j + 3].Value = Client_value[0].Declines_percentage;
                    j = j + 3;
                }/*
                Declines.Cells[rowcount, 2].Value = sortedDict[lcc_count].PerClient[5].Hits;
                    Declines.Cells[rowcount, 3].Value = sortedDict[lcc_count].PerClient[5].Fails;
                    Declines.Cells[rowcount, 4].Value = sortedDict[lcc_count].PerClient[5].Declines;
                    Declines.Cells[rowcount, 5].Value = sortedDict[lcc_count].PerClient[5].Declines_percentage;
                    Declines.Cells[rowcount, 6].Value = sortedDict[lcc_count].PerClient[0].Hits;
                    Declines.Cells[rowcount, 7].Value = sortedDict[lcc_count].PerClient[0].Fails;
                    Declines.Cells[rowcount, 8].Value = sortedDict[lcc_count].PerClient[0].Declines;
                    Declines.Cells[rowcount, 9].Value = sortedDict[lcc_count].PerClient[0].Declines_percentage;
                    Declines.Cells[rowcount, 10].Value = sortedDict[lcc_count].PerClient[1].Hits;
                    Declines.Cells[rowcount, 11].Value = sortedDict[lcc_count].PerClient[1].Fails;
                    Declines.Cells[rowcount, 12].Value = sortedDict[lcc_count].PerClient[1].Declines;
                    Declines.Cells[rowcount, 13].Value = sortedDict[lcc_count].PerClient[1].Declines_percentage;
                    Declines.Cells[rowcount, 14].Value = sortedDict[lcc_count].PerClient[2].Hits;
                    Declines.Cells[rowcount, 15].Value = sortedDict[lcc_count].PerClient[2].Fails;
                    Declines.Cells[rowcount, 16].Value = sortedDict[lcc_count].PerClient[2].Declines;
                    Declines.Cells[rowcount, 17].Value = sortedDict[lcc_count].PerClient[2].Declines_percentage;
                    Declines.Cells[rowcount, 18].Value = sortedDict[lcc_count].PerClient[3].Hits;
                    Declines.Cells[rowcount, 19].Value = sortedDict[lcc_count].PerClient[3].Fails;
                    Declines.Cells[rowcount, 20].Value = sortedDict[lcc_count].PerClient[3].Declines;
                    Declines.Cells[rowcount, 21].Value = sortedDict[lcc_count].PerClient[3].Declines_percentage;
                    Declines.Cells[rowcount, 22].Value = sortedDict[lcc_count].PerClient[4].Hits;
                    Declines.Cells[rowcount, 23].Value = sortedDict[lcc_count].PerClient[4].Fails;
                    Declines.Cells[rowcount, 24].Value = sortedDict[lcc_count].PerClient[4].Declines;
                    Declines.Cells[rowcount, 25].Value = sortedDict[lcc_count].PerClient[4].Declines_percentage;*/
                lcc_count++;
                rowcount++;
            }
            Declines.Row(1).Style.Font.Bold = true;
            Declines.Row(2).Style.Font.Bold = true;
            Declines.Column(1).Style.Font.Bold = true;
            Declines.View.FreezePanes(3,2);
            excel.Save();

        }

        public static string get_Client_string(List<Decline> Client)
        {
            var obj = Client.Select(x => (Decline)x).ToList();
            string Total =null;
            foreach (var client_name in order)
            {
                var Client_value = obj.Where(x => x.Name.Equals(client_name)).ToList();
                Total += string.Join(",",Client_value[0].Hits,Client_value[0].Fails,Client_value[0].Declines,Client_value[0].Declines_percentage);
                Total += ",";
            }
            return Total;
        }
        public static List<string> get_LCC_string(List<PdLcc> Client)
        {
            List<string> order = new List<string> { "Total", "etraveli", "lbftravel", "alternativeairlines", "loveholidays", "flighthub" };
            var obj = Client.Select(x => (PdLcc)x).ToList();
            var results = new List<string>();
            foreach (var lcc in obj)
            {
              string  result = string.Join(",", lcc.Lcc, get_Client_string(lcc.PerClient.ToList()));
                results.Add(result);
            }
            return results;
        }
    }
}
