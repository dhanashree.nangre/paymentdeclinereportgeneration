﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace paymentdeclinereportgeneration
{
    public class ElasticSearchQueries
    {
        public object GetRequestObject( int from_day,int to_day, int dayBuffer = 0)
        {
            DateTime Todate = DateTime.Now.AddDays(-1-from_day);
            Todate = new DateTime(Todate.Year, Todate.Month, Todate.Day, 18,30, 0);
            DateTime fromdate = DateTime.Now.AddDays(-to_day-1);
            fromdate = new DateTime(fromdate.Year, fromdate.Month, fromdate.Day, 18, 30, 0);
            long From_epoch = (long)(fromdate - new DateTime(1970, 1, 1)).TotalMilliseconds;
            long To_epoch = (long)(Todate- new DateTime(1970, 1, 1)).TotalMilliseconds;
            return new PdRequestJson()
            {
                Aggs = new PdRequestJsonAggs()
               {
                    GroupByFailed = new PurpleGroupByFailed()
                    {
                        Aggs = new GroupByFailedAggs()
                        {
                            GroupByStatus = new GroupByStatus()
                            {
                                Terms = new Terms()
                                {
                                    Field = "sixt4.State.Status.keyword",
                                    Size = 100
                                },

                                Aggs = new GroupByStatusAggs()
                                {
                                    GroupByClients = new GroupByClients()
                                    {
                                        Terms = new Terms()
                                        {
                                            Field = "sixt4.State.Tags.ClientName.keyword",
                                            Size = 500
                                        },
                                        Aggs = new GroupByClientsAggs()
                                        {
                                            GroupByFailed = new FluffyGroupByFailed()
                                            {
                                                Terms = new Terms()
                                                {
                                                    Field = "sixt4.Exception.FullName.keyword",
                                                    Size = 100
                                                }
                                            }
                                        }
                                     }
                                }
                            }

                        },

                        Terms = new Terms()
                        {
                                Field = "sixt4.State.Lcc.keyword",
                                Size = 100
                        }
                    }

                },
                Query = new Query()
                {
                    Bool = new Bool()
                    {
                        Must = new List<Must>()
                        {
                            new Must()
                            {
                                QueryString = new QueryString()
                                {
                                    Query = "sixt4.State.Type: SubmitPayment "
                                }
                            },
                            new Must()
                            {
                                Range = new Range()
                                {
                                    Sixt4Timestamp = new Sixt4Timestamp()
                                    {
                                        //Gte = $"now-{dayBuffer + dayRange}d/d",
                                        //Lte = $"now-{dayBuffer}d/d"
                                        Gte =From_epoch.ToString(),//$"1586716200000",
                                        Lte =To_epoch.ToString(),//$"1587320999999",
                                        Format="epoch_millis"
                                    }
                                }
                            }
                        }
                    }
                }
                
            };

        }
    
        public string GetQueryResponse(object queryJson, HttpClient httpClient, string UriString)
        {
            var responseString = string.Empty;

            var jsonstring = JsonConvert.SerializeObject(queryJson);

            var queryURI = UriString + jsonstring;

            using (var request = new HttpRequestMessage(HttpMethod.Get, queryURI))
            {
                using (var response = httpClient.SendAsync(request))
                {
                    responseString = response.Result.Content.ReadAsStringAsync().Result;
                }
            }

            return responseString;
        }


        public void ThrowInvalidResponseAsync(HttpRequestMessage request, HttpResponseMessage response)
        {
            string responseContent = null;

            if (response.Content != null)
            {
                responseContent = response.Content.ReadAsStringAsync().ToString();
            }

            var message = $"Response from {request.Method} {request.RequestUri}: {response.StatusCode} - {responseContent}";

            throw new Exception(message);
        }

        public string GetTBResponse(HttpClient httpClient, string uriString, string apiKey)
        {
            var responseString = string.Empty;

            using (var request = new HttpRequestMessage(HttpMethod.Get, uriString))
            {
                request.Headers.Add("x-api-key", apiKey);

                using (var response = httpClient.SendAsync(request))
                {
                    responseString = response.Result.Content.ReadAsStringAsync().Result;
                }
            }

            return responseString;
        }
    }
}
